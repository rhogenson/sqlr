module gitlab.com/rhogenson/sqlr

go 1.23

require (
	github.com/google/go-cmp v0.6.0
	github.com/mattn/go-sqlite3 v1.14.22
)
